import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy} from '@angular/core';
import { Activity } from '../../shared/models/activity.model';
import { ActivityService } from '../../shared/services/activity.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements AfterViewInit, OnDestroy, OnInit {

  public activities:Activity[];
  public rerenderBool:boolean = false;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};

  constructor(private activityService:ActivityService) { 
  	
  }

  ngOnInit() {
  	
	    
  }

  //init the list of activities and the datatable
  ngAfterViewInit(): void {
  	this.dtOptions = {
      pageLength: 10
    };
    //subscribe to activities
    this.activityService.activities.subscribe(
      (activities: Activity[]) => {
      		//set activity
          this.activities = activities;
          //If already render, make rerender
          if(this.rerenderBool){
          	this.rerender();
          }
          else{
          	this.rerenderBool = true;
          }
        }
      );
      this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  pickActivity(index:number){
    this.activityService.selectActivity(index);
  }

  //reset the datatables with new value
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //delete an activity
  deleteActivity(deleteActivity: Activity){
  	console.log(deleteActivity);
  	if(confirm("Are you sure to delete "+deleteActivity.name+" ?")) {
    	this.activityService.deleteActivity(deleteActivity.id);
  	}
  	
  }

}
