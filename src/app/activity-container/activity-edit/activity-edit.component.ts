import { Component, OnInit } from '@angular/core';
import { FormsModule, FormBuilder, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import { ActivityService } from '../../shared/services/activity.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-activity-edit',
  templateUrl: './activity-edit.component.html',
  styleUrls: ['./activity-edit.component.css']
})
export class ActivityEditComponent implements OnInit {

  public activityForm: FormGroup;
  edit:boolean;
  public addr: object;

  constructor(private activityService:ActivityService, private fb: FormBuilder, private activatedRoute:ActivatedRoute) { }

  ngOnInit() {

  	this.initForm();

  	this.activatedRoute.params.subscribe((params: Params) => {
  		//if a param is set for the route, this is edit
      if (params.index) {
      	this.edit = true;
      	this.activityService.selectActivity(params.index);
        this.activityService.activity.subscribe(activity => {
  			this.initForm({id: activity.id, name: activity.name, desc: activity.desc, place: activity.place, price: activity.price});
		});
      } else {
        this.initForm();
      }

    });
  }

  //init the form with data if edit
  initForm(activity = {id: 0, name: '', desc: '', place: '', price: 0}) {
    this.activityForm = this.fb.group({
      id: activity.id,
      name: [ activity.name, Validators.required],
      desc: activity.desc,
      place: [ activity.place, Validators.required],
      price: [ activity.price, Validators.required],
    });
  }

  //save activity
  saveActivity() {
  	if(this.edit){
    	this.activityService.editActivity(this.activityForm.value);
  	}
  	else{
  		this.activityService.addActivity(this.activityForm.value);
  	}
  }

  //Method to be invoked everytime we receive a new instance 
  //of the address object from the onSelect event emitter.
  setAddress(addrObj) {
    
    this.addr = addrObj.formatted_address;
    console.log(this.addr);
    this.activityForm.controls['place'].setValue(this.addr);

  }

}
