import { Component, OnInit } from '@angular/core';
import { Activity } from '../../shared/models/activity.model';
import { ActivityService } from '../../shared/services/activity.service';
import { ActivatedRoute, Params } from '@angular/router'

@Component({
  selector: 'app-activity-details',
  templateUrl: './activity-details.component.html',
  styleUrls: ['./activity-details.component.css']
})
export class ActivityDetailsComponent implements OnInit {

  public activity:Activity;
  index:number;
  
  constructor(private activityService:ActivityService, private activatedRoute:ActivatedRoute) { 
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params: Params) => {
     if(params.index){
       this.index = params.index;
     }
     else{
       this.index = 0;
     }
     this.activityService.selectActivity(this.index);
     this.activityService.activity.subscribe(
         (activity: Activity) => {
            this.activity = activity;
          }
        );
     });
    
  }

  getUrl() {
    return ['/activities', this.index, 'edit'];
  }

  deleteActivity(deleteActivity: Activity){
  	console.log(deleteActivity);
  	if(confirm("Are you sure to delete "+deleteActivity.name+" ?")) {
    	this.activityService.deleteActivity(deleteActivity.id);
  	}
  	
  }

}
