import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ActivityContainerComponent } from './activity-container/activity-container.component';
import { ActivityListComponent } from './activity-container/activity-list/activity-list.component';
import { ActivityDetailsComponent } from './activity-container/activity-details/activity-details.component';
import { ActivityEditComponent } from './activity-container/activity-edit/activity-edit.component';


const appRoutes: Routes = [
  { path: '', redirectTo: 'activities', pathMatch: 'full'},
  { path: 'activities', component: ActivityContainerComponent, children: [
  	{path: 'new', component: ActivityEditComponent},
  	{path: ':index/edit', component: ActivityEditComponent},
    {path: ':index', component: ActivityDetailsComponent},
    {path: '', component: ActivityDetailsComponent}
  ] }
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}