import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Activity } from '../../shared/models/activity.model';

@Injectable()
export class ActivityService {

  //BehaviorSubject of activities for shared activities with subscriber
  public activities:BehaviorSubject<Activity[]> = new BehaviorSubject<Activity[]>(null);

  //BehaviorSubject of activity for shared activity selected with subscriber
  public activity:BehaviorSubject<Activity> = new BehaviorSubject<Activity>(null);

  //Last id of activity
  public id:number = 0;

  constructor() { 
  	console.log(localStorage.getItem("activities"));
  	
  	//get activities from localStorage
  	let activities = [];
  	if(JSON.parse(localStorage.getItem("activities")) != null){
  		activities = JSON.parse(localStorage.getItem("activities"));
  	}

  	//get last activities id.
  	if(JSON.parse(localStorage.getItem("id")) != null){
  		this.id = JSON.parse(localStorage.getItem("id"));
  	}
  	console.log(this.id);

  	//update behavior subject of activities list
  	this.activities.next(activities);
  	if(this.activities.value != null ){
  		//set behavior subject of activity with the first activity
  		this.activity.next(this.activities.value[0]);
  	}
  }

  //Upadte activity behavior subject with the selected activity
  selectActivity(index:number){
  	if(this.activities.value != null && this.activities.value[index] != null){
  		this.activity.next(this.activities.value[index]);
  	}
  }

  // Add Activity to the behavior Subject activities and in localStorage
  addActivity(activity: Activity) {
    const activities = this.activities.value;
    activities.push( {id: this.id+1, name: activity.name, desc: activity.desc, place: activity.place, price: activity.price});
    this.activities.next(activities);
    localStorage.setItem("activities", JSON.stringify(activities));
    localStorage.setItem("id", JSON.stringify(this.id+1));
  }

  // Edit Activity to the behavior Subject activities and in localStorage
  editActivity(editActivity: Activity) {
    const activities = this.activities.value;
    let index = activities.findIndex(c => c.id === editActivity.id);
    activities[index] = editActivity;
    this.activities.next(activities);
    localStorage.setItem("activities", JSON.stringify(activities));
  }

  // Delete Activity in the behavior Subject activities and in localStorage
  deleteActivity(id: number) {
  	const activities = this.activities.value;
    let index = activities.findIndex(c => c.id === id);
    activities.splice(index,1);
    this.activities.next(activities);
    localStorage.setItem("activities", JSON.stringify(activities));
  }

}
