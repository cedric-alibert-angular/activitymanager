export class Activity{
	public id: number;
	public name: string;
	public desc: string;
	public place: string;
	public price: number;

	constructor(id:number, name: string, desc: string, place: string, price: number) {
		this.id  = id;
		this.name= name;
		this.desc = desc;
		this.place = place;
		this.price = price;
	}
}