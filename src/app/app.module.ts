import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ActivityContainerComponent } from './activity-container/activity-container.component';
import { ActivityListComponent } from './activity-container/activity-list/activity-list.component';
import { ActivityEditComponent } from './activity-container/activity-edit/activity-edit.component';
import { ActivityDetailsComponent } from './activity-container/activity-details/activity-details.component';
import { HeaderComponent } from './header/header.component';

import { ActivityService } from './shared/services/activity.service';
import { AppRoutingModule } from './app-routing.module';

// import de FormsModule :
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Datatable
import { DataTablesModule } from 'angular-datatables';
import { GooglePlacesDirective } from './shared/directives/google-places.directive';

// Font awesome
import { AngularFontAwesomeModule } from 'angular-font-awesome';

@NgModule({
  declarations: [
    AppComponent,
    ActivityContainerComponent,
    ActivityListComponent,
    ActivityEditComponent,
    ActivityDetailsComponent,
    HeaderComponent,
    GooglePlacesDirective
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    AngularFontAwesomeModule
  ],
  providers: [ActivityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
